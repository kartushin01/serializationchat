﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Serialize;

namespace ChatServer
{
    public class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream networkStream { get; private set; }
        string userName;
        TcpClient tcpClient;
        ServerObject serverObj; // объект сервера
        byte[] messageBytes;
        private Message messageObj;


        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            this.tcpClient = tcpClient;
            serverObj = serverObject;
            serverObject.AddConnection(this);
        }

        public void Process()
        {
            try
            {
                networkStream = tcpClient.GetStream();
                // получаем bytes из потока
                messageBytes = SerializeHelper.GetBytesArrayFromStream(networkStream);
                //десериализация в объект
                messageObj = SerializeHelper.DeserializeBytesToObj(messageBytes);

                userName = messageObj.Name;
                messageObj.Text = userName + " вошел в чат";
                
                // посылаем сообщение о входе в чат всем подключенным пользователям
                serverObj.BroadcastMessage(SerializeHelper.GetBytes(messageObj), this.Id);
                Console.WriteLine(messageObj.Text);
                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        //Получить byte[] из потока
                        messageBytes = SerializeHelper.GetBytesArrayFromStream(networkStream);
                        //десериализация в объект
                        messageObj = SerializeHelper.DeserializeBytesToObj(messageBytes);

                        messageObj.Text = String.Format("{0}: {1}", userName, messageObj.Text);
                        Console.WriteLine(messageObj.Text);
                        serverObj.BroadcastMessage(SerializeHelper.GetBytes(messageObj), this.Id);
                    }
                    catch
                    {
                        messageObj.Text = String.Format("{0}: покинул чат", userName);
                        Console.WriteLine(messageObj.Text);
                        serverObj.BroadcastMessage(SerializeHelper.GetBytes(messageObj), this.Id);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                serverObj.RemoveConnection(this.Id);
                Close();
            }
        }

        // закрытие подключения
        protected internal void Close()
        {
            if (networkStream != null)
                networkStream.Close();
            if (tcpClient != null)
                tcpClient.Close();
        }

        
    }
}
