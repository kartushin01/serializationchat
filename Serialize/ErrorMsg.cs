﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serialize
{
    [Serializable]
    public class ErrorMsg
    {
        internal ErrorMsg()
        {
            message = "Ошибка десериализации";
            code = 501;
        }
        public string message { get; set; }
        public int code;
    }
}
