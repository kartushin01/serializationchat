﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serialize
{
    public class SettingsApplication
    {
        public string Host { get; set; }
        public string Port { get; set; }
    }
}
