﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Serialize 
{
    [Serializable]
    public class Message
    {
       
        public Message(string name, string message)
        {
        
            Text = message;
            Name = name;
        }

        [OptionalField]
        public string Name;
        [OptionalField]
        public string Text;

        public ErrorMsg ErrorMsg { get; set; }
        
        public Message()
        {
            throw new NotImplementedException();
        }
    }
}
