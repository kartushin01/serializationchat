﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Serialize
{
   public static class SerializeHelper
    {
       
        public static byte[] GetBytes(object obj)  {

            if (obj == null)
            {
                return null;
            }

            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }

        }

        public static Message DeserializeBytesToObj(byte[] bytes) {

            using (MemoryStream ms = new MemoryStream(bytes))
            {
               
                IFormatter br = new BinaryFormatter();
                try
                {
                    return (Message)br.Deserialize(ms); 
                }
                catch (Exception e)
                {
                    return new Message() { ErrorMsg = new ErrorMsg()};
                }
               
            }

        }

        /// <summary>
        /// Получить массив битов для последующей десериализации в объёкт
        /// </summary>
        /// <returns></returns>
        public static byte[] GetBytesArrayFromStream(Stream Stream)
        {

            byte[] readBuffer = new byte[1024];
            List<byte> outputBytes = new List<byte>();

            int offset = 0;

            while (true)
            {
                int bytesRead = Stream.Read(readBuffer, 0, readBuffer.Length);

                if (bytesRead == 0)
                {
                    break;
                }
                else if (bytesRead == readBuffer.Length)
                {
                    outputBytes.AddRange(readBuffer);
                }
                else
                {
                    byte[] tempBuf = new byte[bytesRead];

                    Array.Copy(readBuffer, tempBuf, bytesRead);

                    outputBytes.AddRange(tempBuf);

                    break;
                }

                offset += bytesRead;
            }

            return outputBytes.ToArray();
        }

        public static SettingsApplication GetSettings()
        {
            string workingDirectory = Environment.CurrentDirectory;
            string slnDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName;
            
            using (StreamReader streamReader = new StreamReader(Path.Combine(slnDirectory, "settings.json")))
            {
                string settingsJson = streamReader.ReadToEnd();
                return JsonConvert.DeserializeObject<SettingsApplication>(settingsJson);
            }

           
        }

    }
}
