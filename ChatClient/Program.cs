﻿using Serialize;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ChatClient
{
    class Program
    {
        static string userName;
        static TcpClient tcpClient;
        static NetworkStream networkStream;
        static private SettingsApplication settingsApplicaton;

        static void Main(string[] args)
        {
            
            settingsApplicaton = SerializeHelper.GetSettings();

            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            tcpClient = new TcpClient();
            try
            {
                tcpClient.Connect(settingsApplicaton.Host, int.Parse(settingsApplicaton.Port)); //подключение клиента
                
                networkStream = tcpClient.GetStream(); // получаем поток
                
                Message messageObj = new Message(userName, null);
                
                byte[] messageBytes = SerializeHelper.GetBytes(messageObj);
                networkStream.Write(messageBytes, 0, messageBytes.Length);

                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //старт потока
                Console.WriteLine("Добро пожаловать, {0}", messageObj.Name);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
        // отправка сообщений
        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");

            while (true)
            {
                string message = Console.ReadLine();

                Message messageObj = new Message(userName, message); 
                
                byte[] messageBytes = SerializeHelper.GetBytes(messageObj);
                
                networkStream.Write(messageBytes, 0, messageBytes.Length);

            }
        }
        // получение сообщений
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] bytesArrayFromStreem = SerializeHelper.GetBytesArrayFromStream(networkStream);
                    Message message = SerializeHelper.DeserializeBytesToObj(bytesArrayFromStreem);
                    if (message.ErrorMsg != null)
                    {
                        Console.WriteLine($"Ошибка {message.ErrorMsg.message}. Код: {message.ErrorMsg.code}");  //вывод сообщеня об ошибке
                    }
                    else
                    {
                        Console.WriteLine(message.Text);  //вывод сообщеня
                    }
                    
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void Disconnect()
        {
            if (networkStream != null)
                networkStream.Close();//отключение потока
            if (tcpClient != null)
                tcpClient.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }
        
        
    }
}
